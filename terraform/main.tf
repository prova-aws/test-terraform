resource "aws_ecr_repository" "my_repository" {
  name = "repo-for-terraform"
}

resource "aws_apigatewayv2_api" "api_gateway" {
  name          = "my-api-gateway"
  protocol_type = "HTTP"
}